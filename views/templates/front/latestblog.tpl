<div class="block">
    <h2 class='sdstitle_block'><a href="http://blogspot.com">{l s='Latest News' mod='smartbloghomelatestnews'}</a></h2>
    <div class="sdsblog-box-content">
        {foreach from=$posts item=post}
            <div id="sds_blog_post" class="col-xs-12 col-sm-4 col-md-3">
                <span class="news_module_image_holder">
                    <a href="{$post.thumbnail}"><img class="feat_img_small" src="{$post.thumbnail}"></a>
                </span><br>
                <span>{$post.date}</span>
                <h4 class="sds_post_title"><a href="{$post.link}">{$post.title}</a></h4>
                <p>
                    {$post.description}
                </p>
                <a href="{$post.link}"  class="r_more">{l s='Read More'}</a>
            </div>
        {/foreach}
    </div>
</div>