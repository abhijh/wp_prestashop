<?php
  $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'cu_posts` (
    `id` int(11) NOT NULL,
    `link` varchar(255) DEFAULT NULL,
    `thumbnail` varchar(255) DEFAULT NULL,
    `title` varchar(255) DEFAULT NULL,
    `description` varchar(255) DEFAULT NULL,
    `date` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
  ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8' ;
?>