<?php
if (!defined('_PS_VERSION_'))
	exit;

class WordpressBlog extends Module
{
	/* @var boolean error */
	protected $_errors = false;
	
	public function __construct()
	{
		$this->name = 'WordpressBlog';
		$this->tab = 'front_office_addon';
		$this->version = '1.0';
		$this->author = 'Abhinav Jha';
		$this->need_instance = 0;

	 	parent::__construct();

		$this->displayName = $this->l('WordpressBlog');
		$this->description = $this->l('Adds a block of latest blog from Wordpress.');
		$this->confirmUninstall = $this->l('Are you sure you want to delete this module?');
	}
	
	public function install()
	{
		if (!parent::install() || !$this->registerHook('displayHome'))
			return false;
		require_once(dirname(__FILE__) . '/sql/install.php');
        if (!Db::getInstance()->Execute($sql))
            return false;
		return true;
	}
	
	public function uninstall()
	{
		if (!parent::uninstall())
			return false;
		require_once(dirname(__FILE__) . '/sql/uninstall.php');
        if (!Db::getInstance()->Execute($sql))
            return false;
        Configuration::deleteByName('ps_cs_endpoint');
        Configuration::deleteByName('ps_cs_username');
        Configuration::deleteByName('ps_cs_password');
		return true;
	}


	public function getContent() {
	    $output = null;
	 
	    if (Tools::isSubmit('submit'.$this->name)) {
	        $ps_cs_endpoint = strval(Tools::getValue('ps_cs_endpoint'));
	        $ps_cs_username = strval(Tools::getValue('ps_cs_username'));
	        $ps_cs_password = strval(Tools::getValue('ps_cs_password'));
	        if (!$ps_cs_endpoint
	          || empty($ps_cs_endpoint)
	          || !$ps_cs_username
	          || empty($ps_cs_username)
	          || !$ps_cs_password
	          || empty($ps_cs_password))
	            $output .= $this->displayError($this->l('Please fill in the information value'));
	        else  {
				Configuration::updateValue('ps_cs_endpoint', $ps_cs_endpoint);
		        Configuration::updateValue('ps_cs_username', $ps_cs_username);
		        Configuration::updateValue('ps_cs_password', $ps_cs_password);	        	
			    $output .= $this->displayConfirmation($this->l('Settings updated'));
	        }
	    }
        return $output.$this->displayForm();
	}


	public function displayForm() {
	    // Get default language
	    $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
	     
	    // Init Fields form array
	    $fields_form[0]['form'] = array(
	        'legend' => array(
	            'title' => $this->l('Settings'),
	        ),
	        'input' => array(
	            array(
	                'type' => 'text',
	                'label' => $this->l('Enter Wordpress XMLRPC endpoint.'),
	                'name' => 'ps_cs_endpoint',
	                'required' => true,
	            ),
	            array(
	                'type' => 'text',
	                'label' => $this->l('Enter Wordpress username.'),
	                'name' => 'ps_cs_username',
	                'required' => true
	            ),
	            array(
	                'type' => 'password',
	                'label' => $this->l('Enter Wordpress password.'),
	                'name' => 'ps_cs_password',
	                'required' => true
	            )
	        ),
	        'submit' => array(
	            'title' => $this->l('Save'),
	            'class' => 'btn btn-default pull-right'
	        )
	    );
	     
	    $helper = new HelperForm();
	     
	    // Module, token and currentIndex
	    $helper->module = $this;
	    $helper->name_controller = $this->name;
	    $helper->token = Tools::getAdminTokenLite('AdminModules');
	    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
	     
	    // Language
	    $helper->default_form_language = $default_lang;
	    $helper->allow_employee_form_lang = $default_lang;
	     
	    // Title and toolbar
	    $helper->title = $this->displayName;
	    $helper->show_toolbar = true;        // false -> remove toolbar
	    $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
	    $helper->submit_action = 'submit'.$this->name;
	    $helper->toolbar_btn = array(
	        'save' =>
	        array(
	            'desc' => $this->l('Save'),
	            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
	            '&token='.Tools::getAdminTokenLite('AdminModules'),
	        ),
	        'back' => array(
	            'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
	            'desc' => $this->l('Back to list')
	        )
	    );
	    // Load current value
	    $helper->fields_value = Configuration::getMultiple(array('ps_cs_endpoint', 'ps_cs_username'));
	    return $helper->generateForm($fields_form);
	}


	public function hookDisplayHome() {
		$this->context->smarty->assign(array(
            'posts' => $this->getPosts(),
        ));
        $this->context->controller->addCSS(__PS_BASE_URI__.'/modules/WordpressBlog/css/wpblog.css', 'all');
        return $this->display(__FILE__, 'views/templates/front/latestblog.tpl');
	}

	public function getPosts() {
		$posts = [];
	    $sql = 'SELECT * FROM '._DB_PREFIX_.'cu_posts ORDER BY id DESC;';
		if ($results = Db::getInstance()->query($sql))
		    foreach ($results as $row) {
		        $post = array(
		        	'id' => $row['id'],
		        	'thumbnail' => $row['thumbnail'],
		        	'title' => $row['title'],
		        	'description' => $row['description'],
		        	'link' => $row['link'],
		        	'date' => date('m-d-Y g:i a', $row['date'])
		        );
		        array_push($posts, $post);
		    }
		return $posts;			
	}
}
