<?php 
	Class WordpressBlogLatestBlogsModuleFrontController extends ModuleFrontController {
		public function init() {
		    $this->page_name = 'latestblog';
		    $this->display_column_left = True;
			$this->display_column_right = false;
			parent::init();
		}
		public function initContent() {
		    parent::initContent();
		    $this->context->smarty->assign(array(
                'posts' => $this->getPosts()
            ));
            $this->addCSS(__PS_BASE_URI__.'/modules/WordpressBlog/css/wpblog.css', 'all');
			$this->setTemplate('latestblog.tpl');
		} 	
		public function getPosts() {
			$posts = [];
		    $sql = 'SELECT * FROM '._DB_PREFIX_.'posts';
			if ($results = Db::getInstance()->query($sql))
			    foreach ($results as $row) {
			        $post = array(
			        	'id' => $row['id'],
			        	'image_url' => $row['image_url'],
			        	'title' => $row['title'],
			        	'description' => $row['description'],
			        	'url' => $row['url']
			        );
			        array_push($posts, $post);
			    }
			return $posts;			
		}
	}
?>