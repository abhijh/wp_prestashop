	<?php 
	require_once('../../config/config.inc.php');
	require_once('../../init.php');
	$conf = Configuration::getMultiple(array('ps_cs_endpoint', 'ps_cs_username', 'ps_cs_password'));
	$endpoint = $conf['ps_cs_endpoint'];
	$username = $conf['ps_cs_username'];
	$password = $conf['ps_cs_password'];
	$db = Db::getInstance();
	$table = 'cu_posts';
	$sql = 'DELETE FROM '._DB_PREFIX_.$table.' WHERE 1=1';
	$db->execute($sql);
	$posts = getPosts($endpoint, $username, $password);
	foreach ($posts as $post) {
	    $post = array(
	    	'id' => (int)$post["post_id"],
	    	'link' => pSQL($post["link"]),
	    	'thumbnail' => pSQL($post["thumbnail"]),
	    	'title' => pSQL($post["post_title"]),
	    	'description' => pSQL($post["post_content"]),
	    	'date' => null
	    );
		$db->insert($table, $post);
	}
	function getPosts($endpoint, $username, $password) {
		$curl = curl_init();
		curl_setopt_array($curl, array(
		    CURLOPT_URL => $endpoint,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_ENCODING => "",
		    CURLOPT_MAXREDIRS => 10,
		    CURLOPT_TIMEOUT => 30,
		    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		    CURLOPT_CUSTOMREQUEST => "POST",
		    CURLOPT_POSTFIELDS => "<?xml version='1.0' encoding='UTF-8'?>
				<methodCall>
				<methodName>wp.getPosts</methodName>
				<params>
				 <param>
				  <value>
				   <int>1</int>
				  </value>
				 </param>
				 <param>
				  <value>
				   <string>$username</string>
				  </value>
				 </param>
				 <param>
				  <value>
				   <string>$password</string>
				  </value>
				 </param>
				 <param>
				  <value>
				   <struct>
				    <member>
				     <name>number</name>
				     <value>
				      <int>4</int>
				     </value>
				    </member>
				    <member>
				     <name>post_status</name>
				     <value>
				      <string>publish</string>
				     </value>
				    </member>
				   </struct>
				  </value>
				 </param>
				</params>
				</methodCall>",
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
		    echo "cURL Error #:" . $err;
		} else {
		    $xml = simplexml_load_string($response) or die("Error: Cannot create object");
		    $data_s = $xml->params->param->value->array->data;
			$posts = [];
  		    for ($i=0; $i < 4; $i++) { 
					$flag = 0;
					$data = $data_s->value[$i]->struct->member;
  		    		$post = [];
  		    		for ($j=0; $j < count($data); $j++) { 
		    			$name = $data[$j]->name;
		    			$value = $data[$j]->value;
  		    			if ($value->string!="") {
  		    				settype($name, "string");
  		    				$post[$name] = $data[$j]->value->string;
  		    			} elseif ($name == 'post_thumbnail') {
  		    				$thumbnail_contet = $value->struct->member;
  		    				if ($thumbnail_contet == null) {
  		    					$post['thumbnail'] = __PS_BASE_URI__.'modules/WordpressBlog/images/no_thumbnail.jpg';
  		    				} else 
	  		    				foreach ($thumbnail_contet as $key => $value) {
	  		    					if ($value->name == 'thumbnail') {
	  		    					 	$post['thumbnail'] = $value->value->string;
	  		    					}
	  		    				}
  		    			} else if ($name == 'custom_fields') {
  		    				$youst_content_ = $value->array->data->value;
							foreach ($youst_content_ as $item) {
								$youst_content = $item->struct->member;
									if($youst_content[1]->value->string == "_yoast_wpseo_opengraph-description")
										$post['post_content'] = $youst_content[2]->value->string;
							}
  		    			}
  		    		}
  		    		array_push($posts, $post);
		    }			    
		}	
		return $posts;
	}
?>